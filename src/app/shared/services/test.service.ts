import {Injectable} from '@angular/core';

import {ApiService} from "./api.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";


@Injectable()
export class TestService extends ApiService {

    constructor (protected http: HttpClient,
                 protected router: Router)
    {
        super(http, router);
    }

    getTestPage() {
        return this.get('pages/slug/about', {});
    }
}
