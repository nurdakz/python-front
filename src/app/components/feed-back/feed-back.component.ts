import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TestService} from "../../shared/services/test.service";

@Component({
  selector: 'app-feed-back',
  templateUrl: './feed-back.component.html',
  styleUrls: ['./feed-back.component.css']
})
export class FeedBackComponent implements OnInit {

  feedBackForm: FormGroup;

  constructor(private router: Router, private testService: TestService) {
      this.feedBackForm = new FormGroup({
          'message': new FormControl(null, [Validators.required]),
          'email': new FormControl(null, [Validators.required, Validators.email]),
      });
  }

  ngOnInit() {

  }

  sendFeedBack() {
    const form = this.feedBackForm.getRawValue();
    console.log(form);
  }

}
