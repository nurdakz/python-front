import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MailPageComponent} from "./components/mail-page/mail-page.component";
import {FeedBackComponent} from "./components/feed-back/feed-back.component";
import {SearchFormComponent} from "./components/search-form/search-form.component";
const routes: Routes = [
    {path: '', component: MailPageComponent, pathMatch: 'full'},
    {path: 'feed-back', component: FeedBackComponent},
    {path: 'search', component: SearchFormComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        onSameUrlNavigation: 'reload', useHash: true
    })],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
