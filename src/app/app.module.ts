import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MailPageComponent } from './components/mail-page/mail-page.component';
import { FeedBackComponent } from './components/feed-back/feed-back.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {ApiService} from "./shared/services/api.service";
import {TestService} from "./shared/services/test.service";
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SitebarComponent } from './layouts/sitebar/sitebar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HeaderComponent } from './layouts/header/header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    MailPageComponent,
    FeedBackComponent,
    SearchFormComponent,
    SitebarComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
      NgbModule
  ],
  providers: [
      ApiService,
      TestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
