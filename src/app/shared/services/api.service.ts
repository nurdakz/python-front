import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";


@Injectable()
export class ApiService {
    API_URL = environment.apiUrl;
    URL = environment.url;

    constructor(
        protected http: HttpClient,
        protected router: Router
    ) {}

    public get(uri: string, body: any): Promise<any> {
        body = this.normalBody(body);
        const pars = this.getUrlParams(body);
        return this.http.get(this.API_URL + uri, {params: pars})
            .toPromise()
            .catch((error: HttpErrorResponse) => {
                console.log(error);
            });
    }

    public post(uri: string, body: any): Promise<any> {
        body = this.normalBody(body);
        return this.http.post(this.API_URL + uri, body).toPromise()
            .catch((error: HttpErrorResponse) => {
                console.log(error);
            });
    }

    public delet(uri: string, body: any): Promise<any> {
        body = this.normalBody(body);
        return this.http.delete(this.API_URL + uri, body).toPromise().then(res => res);
    }

    public put(uri: string, body: any): Promise<any> {
        body = this.normalBody(body);
        return this.http.put(this.API_URL + uri, body).toPromise().then(res => res);
    }

    private getUrlParams(body: any): HttpParams {
        let params = new HttpParams();
        for (const key in body) {
            if (!body.hasOwnProperty(key)) {
                continue;
            }
            params = params.append(key, body[key]);
        }
        return params;
    }

    private normalBody(body: any): any {
        if (!body) {
            body = {};
        }

        return body;
    }
}
